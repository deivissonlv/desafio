import styled from 'styled-components/native';

export const  Container = styled.View `
  padding : 20px;
  borer-radius:4px;
  background: #FFF;
  margin-bottom:15px;
`;

export const Name = styled.Text `
font-size:20px;
font-weight: bold;
color:#333;

`;
export const Description= styled.Text.attrs({
    numberOfLines:3,
}) `
font-size:15px;
color:#666;
margin-top:5px;
line-height:20px;
`;
export const RealeaseDate = styled.Text `
font-size:10px;
color:#666;
`;

