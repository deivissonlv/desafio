import React from 'react';
import { View } from 'react-native';

import { Container,Name,Description,RealeaseDate } from './styles';

export default function Films({data}) {
  return (
   <Container>
       <Name>{data.original_title}</Name>
       <RealeaseDate>data.release_date</RealeaseDate>
       <Description>data.overview</Description>
     

   </Container>
  );
}
