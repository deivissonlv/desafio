import{createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Login from './pages/Login/Login'
import CreateAccount  from '../src/pages/CreateAccount/CreateAccount'
import List from '../src/pages/List/List';




const AuthStack = createStackNavigator({
    Login,
    CreateAccount
});


const ListStack = createStackNavigator({
    List,
});


export default createAppContainer(createSwitchNavigator({
    Auth:AuthStack,
    List:ListStack, 
  }))
  



