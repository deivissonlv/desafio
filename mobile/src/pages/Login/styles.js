import styled from 'styled-components/native';
import {LinearGradient} from 'expo-linear-gradient'


export const Container = styled(LinearGradient).attrs({
    colors: ['#ed0772', '#00aaff'],
    start: { x: 0, y: 0 },
    end: { x:1 , y: 1.5 },
  })`
    flex: 1;
   
    align-items: center;
    
  `;
  export const Title = styled.Text `
  font-size: 32px;
  color: #FFF;
  fontWeight: bold;
  padding: 0  20px;

`;
export const Logo = styled.Image `

`;

export const Form= styled.View `
align-self: stretch;
padding-horizontal: 30px;
margin-top: 120px;
`;

export const Label  = styled.Text `
font-weight: bold;
color: #fff;


`;

export const LabelBtn  = styled.Text `
font-weight: bold;
color: #1cb2c9;

`;

export const Input = styled.TextInput `

    border-width: 1px;
    border-color: #ddd;
    padding-horizontal: 20px;
    font-size: 16px;
    color: #fff;
    height: 44px;
    margin-bottom: 20px;
    border-radius: 2px;
  ,
`;

export const Submit = styled.TouchableOpacity `
height: 42px;
background: #fff;
justify-content: center;
border-radius: 2px;
align-items:center;
margin-bottom:15px;

`;

export const Cadastro = styled.TouchableOpacity `
height: 42px;

justify-content: center;
border-radius: 2px;
align-items:center;
margin-bottom:15px;

`;
/////////////////////////////////////////////////////////////////////////////////////////
export const Search = styled.View `
  flex-direction: row;
  margin-top:10px;
  padding : 0 20px;

`;

export const InputSearch = styled.TextInput.attrs({
  placeHolderColor: '#FFF'
})`
flex:1;
padding:12px 15px;
border-radius:4px;
font-size:16px;
color:#333;
background:#FFF;

`;


export const SubmitFilm = styled.TouchableOpacity `
background:#00aaff;
margin-left:10px;
justify-content:center;
border-radius:4px;
padding:0 16px;
`;

export const Lista= styled.FlatList.attrs({
  contentContainerStyle:{paddingHorizontal:20},
  showsVerticalScrollIndicator: false,
}) `
margin-top :20px;



`;
