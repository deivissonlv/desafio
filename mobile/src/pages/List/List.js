import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons'
import {Container, Title, Search, InputSearch,SubmitFilm, Lista} from '../Login/styles';

import Films from '../../components/Films/Index';
import api from '../../services/api'

export default function List(){

    const[input,setInput] = useState('');
    const [films,setFilms] = useState([]);

   async function searchFilm(){
        try{
            const response = await api.get(`/search/movie?api_key=f8ac775db0591fd84d17a65c6c0bb596&language=en-US&query=${input}`)
            setFilms(response.data.results)
            console.log(response.data.results)
        }catch(error){
            Alert.alert("Não foi possível carregar os Filmes," + " motivo: " + error.response.data.error);


        }

    }

        
    
    return(
        <Container>
                <Title>Buscar</Title>
                
                <Search>
                    <InputSearch
                    value={input}
                    onChangeText={setInput}
                        autoCapitalize="none"
                        autoCorrect={false}
                        placeHolder="Procurar Filmes"
                    />
                

                <SubmitFilm onPress={searchFilm}>
                    <Icon name="search" size ={22}  color="#FFF"/>
                </SubmitFilm>
                </Search>



                <Lista
          keyboardShouldPersistTaps="handled"
         data={films}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => (
          <Repository data={item}/>
        )}
      />
        </Container>
    )
}